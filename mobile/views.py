import os

from django.shortcuts import render
from django.urls import URLResolver
from rest_framework.decorators import api_view
from rest_framework.response import Response
from fcm_django.models import FCMDevice as Device

from news.services import MainScheduler


@api_view(['GET'])
def get_all_methods(request, **kwargs):
    from .urls import urlpatterns
    urls = []
    for url_obj in urlpatterns:
        if isinstance(url_obj, URLResolver):
            for url in url_obj.url_patterns:
                urls.append({str(url.name): str(url_obj.pattern) + str(url.pattern)})
        else:
            url_line = url_obj.pattern
            urls.append({url_line.name: str(url_line)})
    return Response(data=urls, status=200)


@api_view(['POST'])
def register_device(request, **kwargs):
    key = request.data.get('key')

    if key not in map(lambda x: x.registration_id, Device.objects.all()):
        Device.objects.create(registration_id=key, active=True)
        return Response(data={'result': 'ok'}, status=200)
    else:
        return Response(data={'result': 'fail'}, status=400)


def notify(request, *args, **kwargs):
    context = {
        'notify_res': 0,
        'cron_res': 0
    }
    if request.method == 'POST':
        password = request.POST.get('password')
        title = request.POST.get('title')
        body = request.POST.get('body')
        cron = request.POST.get('cron')
        with open(os.path.dirname(os.path.realpath(__file__)) + '/pass.txt', 'r') as file:
            real_pass = file.read()
        if password == real_pass:
            if not cron:
                Device.objects.all().send_message(title=title, body=body)
                context['notify_res'] = 1
                return render(request, 'form.html', context)
            else:
                try:
                    MainScheduler.modify_cron(cron)
                except Exception as e:
                    print(e)
                    context['cron_res'] = -1
                    return render(request, 'form.html', context)
                context['cron_res'] = 1
                return render(request, 'form.html', context)
        else:
            context['notify_res'] = -1
            return render(request, 'form.html', context)
    else:
        return render(request, 'form.html', context)
