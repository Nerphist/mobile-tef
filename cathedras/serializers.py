from rest_framework.serializers import ModelSerializer

from cathedras.models import Cathedra


class CathedraSerializer(ModelSerializer):
    class Meta:
        model = Cathedra
        fields = '__all__'
