from django.apps import AppConfig


class CathedrasConfig(AppConfig):
    name = 'cathedras'
