from django.db import models


# Create your models here.
class Cathedra(models.Model):
    name = models.CharField(unique=True, max_length=255)
    description = models.TextField()
