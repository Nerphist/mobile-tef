from rest_framework import generics

# Create your views here.
from cathedras.models import Cathedra
from cathedras.serializers import CathedraSerializer


class CathedraListView(generics.RetrieveAPIView):
    serializer_class = CathedraSerializer

    def get_object(self):
        try:
            id = int(self.kwargs.get('id'))
        except Exception as e:
            id = None
        if id:
            return Cathedra.objects.filter(id=id).first()
        else:
            return None
