from django.conf.urls import url

from cathedras import views

urlpatterns = [
    url(r'^(?P<id>\d+)/$', view=views.CathedraListView.as_view(), name='get cathedra by id'),
]
