import datetime

from django.db import models


# Create your models here.
class News(models.Model):
    created = models.DateTimeField(default=datetime.datetime.now)
    title = models.CharField(unique=True, max_length=255)
    content = models.TextField()
    path_to_photo = models.CharField(max_length=100, default='news/newsparser/newsparser/static/img/tef.png')

    design_types = (
        (1, 'Full news'),
        (2, 'Regular news'),
    )

    design_type = models.IntegerField(choices=design_types, default=1)
