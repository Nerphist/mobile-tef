import dateutil.parser as parser
from rest_framework import generics
from news.paginators import NewsPaginator
from news.serializers import NewsLatestSerializer, NewsPhotoSerializer, NewsContentSerializer
from news.services import NewsHolder


class NewsList(generics.ListAPIView):
    serializer_class = NewsLatestSerializer
    pagination_class = NewsPaginator

    def get_queryset(self):
        return list(sorted(NewsHolder.news.values(),
                           key=lambda x: x.created, reverse=True))


class NewsGetByID(generics.RetrieveAPIView):

    def get_object(self):
        return NewsHolder.news.get(int(self.kwargs['id']))


class NewsListBeforeDate(generics.ListAPIView):

    def get_queryset(self):
        size = 10
        if 'size' in self.request.query_params:
            size = int(self.request.query_params.get('size'))
        print(size)
        date = parser.parse(self.kwargs.get('date'))
        return list(sorted(filter(lambda x: x.created <= date, NewsHolder.news.values()),
                           key=lambda x: x.created, reverse=True))[:size]


class NewsPhotoById(NewsGetByID):
    serializer_class = NewsPhotoSerializer


class NewsContentById(NewsGetByID):
    serializer_class = NewsContentSerializer


class NewsContentListByDate(NewsListBeforeDate):
    serializer_class = NewsContentSerializer


class NewsPhotoListByDate(NewsListBeforeDate):
    serializer_class = NewsPhotoSerializer
