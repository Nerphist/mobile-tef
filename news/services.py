import datetime
import os
import time
import subprocess
import logging

from apscheduler.schedulers.background import BackgroundScheduler

from news.models import News
from fcm_django.models import FCMDevice as Device


class MainScheduler:
    scheduler = BackgroundScheduler()

    @staticmethod
    def news_sync():
        NewsSynchronizer.total_sync(notify=False)
        MainScheduler.total_sync = MainScheduler.scheduler.add_job(NewsSynchronizer.total_sync, trigger='cron',
                                                                   minute='*/15')
        MainScheduler.scheduler.start()

    @staticmethod
    def modify_cron(minutes):
        MainScheduler.scheduler.remove_job(MainScheduler.total_sync.id)
        MainScheduler.total_sync = MainScheduler.scheduler.add_job(NewsSynchronizer.total_sync, trigger='cron',
                                                                   minute=minutes)
        print(MainScheduler.total_sync)
        return True


class NewsSynchronizer:

    @staticmethod
    def sync_sites_with_db():
        logging.info('SYNC STARTED')
        processes, error = subprocess.Popen('ps aux | grep scrapy', shell=True,
                                            stdout=subprocess.PIPE).communicate()
        for proc in processes.decode('ascii').split('\n'):
            name = ' '.join(proc.split(' ')[-2:])
            if name == 'crawl tef' or name == 'crawl pkkpi':
                logging.info('ALREADY SYNCING')
                return
        os.system(
            ' cd /usr/src/app/news/newsparser/newsparser/ && '
            'scrapy crawl tef')
        os.system(
            'cd /usr/src/app/news/newsparser/newsparser/ && '
            'scrapy crawl pkkpi')

    @staticmethod
    def sync_local_with_db(notify=True):
        current_news_list = News.objects.all().order_by('-created')
        if len(NewsHolder.news) == len(current_news_list):
            return True
        for news in current_news_list:
            if news.title not in map(lambda x: x.title, NewsHolder.news.values()):
                if notify:
                    Device.objects.all().send_message(title='Новина', body=news.title.strip())
                NewsHolder.news.update({news.id: news})
        return True

    @staticmethod
    def total_sync(notify=True):
        NewsSynchronizer.sync_sites_with_db()
        time.sleep(10)
        NewsSynchronizer.sync_local_with_db()


class NewsHolder:
    news = {}
