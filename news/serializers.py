import base64

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from news.models import News


class NewsLatestSerializer(ModelSerializer):
    about = serializers.SerializerMethodField()

    def get_about(self, news):
        about = '' if news.design_type != 1 else ' '.join(news.content[:255].split(' ')[:-2])
        return about

    class Meta:
        model = News
        fields = ('id', 'created', 'title', 'about', 'design_type', 'path_to_photo')


class NewsPhotoSerializer(ModelSerializer):
    # photo_base64 = serializers.SerializerMethodField()
    #
    # def get_photo_base64(self, news):
    #     with open(news.path_to_photo, "rb") as image_file:
    #         encoded_string = base64.b64encode(image_file.read())
    #     encoded_photo = encoded_string.decode()
    #     return encoded_photo

    class Meta:
        model = News
        fields = ('path_to_photo',)


class NewsContentSerializer(ModelSerializer):
    class Meta:
        model = News
        fields = ('content',)
