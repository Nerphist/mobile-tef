# -*- coding: utf-8 -*-
import os

BOT_NAME = 'newsparser'

SPIDER_MODULES = ['newsparser.spiders']
NEWSPIDER_MODULE = 'newsparser.spiders'

# Obey robots.txt rules
ROBOTSTXT_OBEY = True

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
# SPIDER_MIDDLEWARES = {
#    'newsparser.middlewares.NewsparserSpiderMiddleware': 543,
# }

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'newsparser.middlewares.NewsparserDownloaderMiddleware': 543,
}

ITEM_PIPELINES = {
    'newsparser.pipelines.FinalNewsPipeline': 400,
}

IMAGES_STORE = 'static/img/'

MONGO_URI = os.environ.get('MONGO_URL', '127.0.0.1') + ':27017',

MONGO_DATABASE = 'testdb'

LOG_LEVEL = 'INFO'
