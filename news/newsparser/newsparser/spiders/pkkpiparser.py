import datetime

import scrapy


class PkKPISpider(scrapy.Spider):
    name = 'pkkpi'
    start_urls = ['https://pk.kpi.ua/news/']

    def parse(self, response: scrapy.http.Response):
        articles = response.css('article')
        for article in articles:
            image = article.css('div.newspage-img-block>a>img').attrib.get('src')
            title = article.css('div.newspage-main>a>header::text').get()
            about = article.css('div.newspage-main>div.newspage-text>p::text').get()
            news_href = article.css('div.newspage-img-block>a').attrib.get('href')
            created = datetime.datetime.strptime(article.css('time::text').get(), '%d.%m.%Y')
            req = scrapy.Request(url=news_href, callback=self.parse_content)
            req.meta['image'] = image
            req.meta['title'] = title
            req.meta['about'] = about
            req.meta['created'] = created
            yield req

    def parse_content(self, response: scrapy.http.Response):
        content = response.css('div.single-post-content')[0].get()
        yield {
            'image': response.meta['image'],
            'title': response.meta['title'],
            'about': response.meta['about'],
            'content': content,
            'created': response.meta['created']
        }
