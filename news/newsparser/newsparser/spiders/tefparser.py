import re
from urllib.parse import urljoin

import scrapy
import datetime


class TefSpider(scrapy.Spider):
    name = 'tef'
    start_urls = ['http://tef.kpi.ua/news']

    def parse(self, response: scrapy.http.Response):
        articles = response.css('.container article')
        for article in articles:
            image = article.css('a>img').attrib.get('src')
            title = article.css('h5>a::text').get()
            news_href = article.css('h5>a').attrib.get('href')
            req = scrapy.Request(url=urljoin(self.start_urls[0], news_href), callback=self.parse_content)
            req.meta['image'] = urljoin(self.start_urls[0], image)
            req.meta['title'] = title
            if image and title and news_href:
                yield req

    def parse_content(self, response: scrapy.http.Response):
        content = response.css('.container article')[0].get()
        content = '<article>' + content[content.find('</ul>') + 5:].strip()
        try:
            created = datetime.datetime.strptime(response.css('time::text').get(), '%Y-%m-%d')
        except ValueError as e:
            try:
                created = datetime.datetime.strptime(response.css('time::text').get(), '%d-%m-%Y')
            except ValueError as e:
                created = datetime.datetime.now()
        yield {
            'image': response.meta['image'],
            'title': response.meta['title'],
            'about': content[:255],
            'content': content,
            'created': created
        }
