# -*- coding: utf-8 -*-


import pymongo
from scrapy import Request
from scrapy.exceptions import DropItem
from scrapy.pipelines.images import ImagesPipeline


class MyImagesPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        yield Request(item['image'])

    def item_completed(self, results, item, info):
        image_paths = [info['path'] for success, info in results if success]
        if not image_paths:
            raise DropItem("Item contains no images")
        item.update({
            'design_type': 1
        })
        return item


class FinalNewsPipeline:
    collection_name = 'news_news'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        item['path_to_photo'] = item['image']
        item.pop('image')
        highest_id = self.db[self.collection_name].find().sort([('id', pymongo.DESCENDING)]).limit(1)
        highest_id = highest_id.next().get('id') if highest_id.count() else 1
        item.update({
            'id': int(highest_id) + 1,
            'design_type': 1,
        })
        try:
            self.db[self.collection_name].insert_one(dict(item))
        except Exception as e:
            item.pop('id')
            self.db[self.collection_name].update_one({'created': item['created']}, {'$set': item})
        return item
