from django.conf.urls import url

from news.views import NewsList, NewsContentById, NewsPhotoById, NewsContentListByDate, NewsPhotoListByDate

urlpatterns = [
    url(r'^latest/$', view=NewsList.as_view(), name='latest news'),
    url(r'^content/(?P<id>\d+)/$', view=NewsContentById.as_view(), name='content by id'),
    url(r'^photo/(?P<id>\d+)/$', view=NewsPhotoById.as_view(), name='photo by id'),
    url(r'^content/(?P<date>\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(\.\d+|)([+-][0-2]\d:[0-5]\d|Z))/$',
        name='content list by date',
        view=NewsContentListByDate.as_view()),
    url(r'^photo/(?P<date>\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(\.\d+|)([+-][0-2]\d:[0-5]\d|Z))/$',
        view=NewsPhotoListByDate.as_view(), name='photo list by date'),
]
