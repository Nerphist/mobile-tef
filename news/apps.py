from django.apps import AppConfig


class NewsConfig(AppConfig):
    name = 'news'

    def ready(self):
        from .services import MainScheduler as scheduler
        scheduler.news_sync()
